package in.forsk.listactivitywithcursoradapter_sqlite.DbAdapter;

//
//
//import java.io.File;
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.OutputStream;
//import java.util.ArrayList;
//
//import com.ntpl.Lync.MyApplication;
//import com.ntpl.Lync.R;
//
//import android.content.ContentValues;
//import android.content.Context;
//import android.database.Cursor;
//import android.database.SQLException;
//import android.database.sqlite.SQLiteDatabase;
//import android.database.sqlite.SQLiteException;
//import android.database.sqlite.SQLiteOpenHelper;
//import android.util.Log;
//
//public class DBAdapter extends SQLiteOpenHelper {
//
//	private static String DB_PATH = "";
//	private static int DB_VESRION = 1;
//	private static final String DB_NAME = "lyncz.db";
//	private SQLiteDatabase myDataBase;
//	private final Context myContext;
//
//    public static final String TABLE_CONTACT	=	"contact";
//
//    
//	private static DBAdapter mDBConnection;
//
//	/**
//	 * Constructor Takes and keeps a reference of the passed context in order to
//	 * access to the application assets and resources.
//	 * 
//	 * @param context
//	 */
//	private DBAdapter(Context context) {
//		super(context, DB_NAME, null, DB_VESRION);
//
//		this.myContext = context;
//		DB_PATH = "/data/data/"+ context.getApplicationContext().getPackageName()+ "/databases/";
//	}
//
//	/**
//	 * getting Instance
//	 * 
//	 * @param context
//	 * @return DBAdapter
//	 */
//	public static synchronized DBAdapter getDBAdapterInstance(Context context) {
//		if (mDBConnection == null) {
//			mDBConnection = new DBAdapter(context);
//		}
//		return mDBConnection;
//	}
//
//	/**
//	 * Creates an empty database on the system and rewrites it with your own
//	 * database.
//	 **/
//	public void createDataBase() throws IOException
//	{
//	    //If database not exists copy it from the assets
//
//		 boolean dbExist = checkDataBase();
//
//		 
//		 
//         if(dbExist){
//             // By calling this method here onUpgrade will be called on a
//             // writable database, but only if the version number has been increased
//             this.getWritableDatabase();
//         }
//
//         
//	    boolean mDataBaseExist = checkDataBase();
//	    if(!mDataBaseExist)
//	    {
//	    	//By calling this method an empty database will be created into the default system path
//            //of the application so we will be able to overwrite that database with our database.
//	        this.getReadableDatabase();
//	        this.close();
//	        try 
//	        {
//	            //Copy the database from assests
//	            copyDataBase();
//	            Log.e("Db Adapter", "createDatabase database created");
//	        } 
//	        catch (IOException mIOException) 
//	        {
//	            throw new Error("ErrorCopyingDataBase");
//	        }
//	    }
//	}
//
//	/**
//	 * Check if the database already exist to avoid re-copying the file each
//	 * time you open the application.
//	 * 
//	 * @return true if it exists, false if it doesn't
//	 */
//	private boolean checkDataBase()
//    {
//        File dbFile = new File(DB_PATH + DB_NAME);
//        //Log.v("dbFile", dbFile + "   "+ dbFile.exists());
//        return dbFile.exists();
//    }
//
//
//	/**
//	 * Copies your database from your local assets-folder to the just created
//	 * empty database in the system folder, from where it can be accessed and
//	 * handled. This is done by transfering bytestream.
//	 * */
//	// public void copyDataBase() throws IOException {
//	// // Open your local db as the input stream
//	// InputStream myInput = myContext.getAssets().open(DB_NAME);
//	// // Path to the just created empty db
//	// String outFileName = DB_PATH + DB_NAME;
//	// // Open the empty db as the output stream
//	// OutputStream myOutput = new FileOutputStream(outFileName);
//	// // transfer bytes from the inputfile to the outputfile
//	// byte[] buffer = new byte[1024];
//	// int length;
//	// while ((length = myInput.read(buffer)) > 0) {
//	// myOutput.write(buffer, 0, length);
//	// }
//	// // Close the streams
//	// myOutput.flush();
//	// myOutput.close();
//	// myInput.close();
//	// }
//
//	public static void copyDataBase() throws IOException {
//		// Open your local db as the input stream
//
//		String DB_PATH = "/data/data/"
//				+ MyApplication.getContext().getApplicationContext()
//						.getPackageName() + "/databases/";
//		InputStream myInput = MyApplication.getContext().getResources()
//				.openRawResource(R.raw.lync_db);
//
//		// getContext().getAssets().open(DB_NAME);
//		// Path to the just created empty db
//		String outFileName = DB_PATH + DB_NAME;
//		// Open the empty db as the output stream
//		OutputStream myOutput = new FileOutputStream(outFileName);
//		// transfer bytes from the inputfile to the outputfile
//		byte[] buffer = new byte[1024];
//		int length;
//		while ((length = myInput.read(buffer)) > 0) {
//			myOutput.write(buffer, 0, length);
//		}
//		// Close the streams
//		myOutput.flush();
//		myOutput.close();
//		myInput.close();
//	}
//
//	/**
//	 * Open the database
//	 * 
//	 * @throws SQLException
//	 */
//	public SQLiteDatabase openDataBase() throws SQLException {
//
////		synchronized (lock) {
//			String myPath = DB_PATH + DB_NAME;
//			File dbFile = myContext.getDatabasePath(myPath);
//
//			if(dbFile.exists() && myDataBase == null){
//				myDataBase = SQLiteDatabase.openDatabase(myPath, null,
//						SQLiteDatabase.CREATE_IF_NECESSARY);
//			}else {
//				System.out.println(">>>>Datebase Does not exists or datebase object already have read&Write access");
//			}
////		}
//		return myDataBase;
//	}
//
//	/**
//	 * Close the database if exist
//	 */
//	@Override
//	public synchronized void close() {
//			if (myDataBase != null)
//				myDataBase.close();
//		super.close();
//	}
//
//
//	/**
//	 * Call on creating data base for example for creating tables at run time
//	 */
//	@Override
//	public void onCreate(SQLiteDatabase db) {
//		// TODO Auto-generated method stub
//		System.out.println("SQLiteDatabase onCreate");
//	}
//
//	@Override
//	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
//		System.out.println("SQLiteDatabase onUpgrade");
//
//		try {
//			copyDataBase();
//		} catch (IOException e) {
//			System.out.println("Error in updating Database");
//			e.printStackTrace();
//		}
//	}
//
//	// ----------------------- CRUD Functions ------------------------------
//
//	/**
//	 * This function used to select the records from DB.
//	 * 
//	 * @param tableName
//	 * @param tableColumns
//	 * @param whereClase
//	 * @param whereArgs
//	 * @param groupBy
//	 * @param having
//	 * @param orderBy
//	 * @return A Cursor object, which is positioned before the first entry.
//	 */
//	public Cursor selectRecordsFromDB(String tableName, String[] tableColumns,
//			String whereClase, String whereArgs[], String groupBy,
//			String having, String orderBy) {
//
//		return myDataBase.query(tableName, tableColumns, whereClase, whereArgs,
//				groupBy, having, orderBy);
//	}
//
//	/**
//	 * select records from db and return in list
//	 * 
//	 * @param tableName
//	 * @param tableColumns
//	 * @param whereClase
//	 * @param whereArgs
//	 * @param groupBy
//	 * @param having
//	 * @param orderBy
//	 * @return ArrayList<ArrayList<String>>
//	 */
//	// public ArrayList<ArrayList<String>> selectRecordsFromDBList(String
//	// tableName, String[] tableColumns,
//	// String whereClase, String whereArgs[], String groupBy,
//	// String having, String orderBy) {
//	//
//	// ArrayList<ArrayList<String>> retList = new
//	// ArrayList<ArrayList<String>>();
//	//
//	// ArrayList<String> list = new ArrayList<String>();
//	//
//	// Cursor cursor = myDataBase.query(tableName, tableColumns, whereClase,
//	// whereArgs,
//	// groupBy, having, orderBy);
//	//
//	// if (cursor.moveToFirst()) {
//	// do {
//	// list = new ArrayList<String>();
//	//
//	// for(int i=0; i<cursor.getColumnCount(); i++){
//	// list.add( cursor.getString(i) );
//	// }
//	// retList.add(list);
//	// } while (cursor.moveToNext());
//	// }
//	// if (cursor != null && !cursor.isClosed()) {
//	// cursor.close();
//	// }
//	// return retList;
//	//
//	// }
//
//	/**
//	 * This function used to insert the Record in DB.
//	 * 
//	 * @param tableName
//	 * @param nullColumnHack
//	 * @param initialValues
//	 * @return the row ID of the newly inserted row, or -1 if an error occurred
//	 */
//	public long insertRecordsInDB(String tableName, String nullColumnHack,
//			ContentValues initialValues) {
//		myDataBase	=	this.getWritableDatabase();
//		return myDataBase.insert(tableName, nullColumnHack, initialValues);
//	}
//
//	/**
//	 * This function used to update the Record in DB.
//	 * 
//	 * @param tableName
//	 * @param initialValues
//	 * @param whereClause
//	 * @param whereArgs
//	 * @return true / false on updating one or more records
//	 */
//	public boolean updateRecordInDB(String tableName,
//			ContentValues initialValues, String whereClause, String whereArgs[]) {
//		myDataBase	=	this.getWritableDatabase();
//		return myDataBase.update(tableName, initialValues, whereClause,
//				whereArgs) > 0;
//	}
//
//	/**
//	 * This function used to update the Record in DB.
//	 * 
//	 * @param tableName
//	 * @param initialValues
//	 * @param whereClause
//	 * @param whereArgs
//	 * @return 0 in case of failure otherwise return no of row(s) are updated
//	 */
//	public int updateRecordsInDB(String tableName, ContentValues initialValues,
//			String whereClause, String whereArgs[]) {
//		myDataBase	=	this.getWritableDatabase();
//		return myDataBase.update(tableName, initialValues, whereClause,
//				whereArgs);
//	}
//
//	/**
//	 * This function used to delete the Record in DB.
//	 * 
//	 * @param tableName
//	 * @param whereClause
//	 * @param whereArgs
//	 * @return 0 in case of failure otherwise return no of row(s) are deleted.
//	 */
//	public int deleteRecordInDB(String tableName, String whereClause,
//			String[] whereArgs) {
//		myDataBase	=	this.getWritableDatabase();
//		return myDataBase.delete(tableName, whereClause, whereArgs);
//	}
//
//	// --------------------- Select Raw Query Functions ---------------------
//
//	/**
//	 * apply raw Query
//	 * 
//	 * @param query
//	 * @param selectionArgs
//	 * @return Cursor
//	 */
//	public Cursor selectRecordsFromDB(String query, String[] selectionArgs) {
//		myDataBase	=	this.getWritableDatabase();
//		return myDataBase.rawQuery(query, selectionArgs);
//	}
//
//	/**
//	 * apply raw query and return result in list
//	 * 
//	 * @param query
//	 * @param selectionArgs
//	 * @return ArrayList<ArrayList<String>>
//	 */
//	public ArrayList<ArrayList<String>> selectRecordsFromDBList(String query,
//			String[] selectionArgs) {
//		myDataBase	=	this.getWritableDatabase();
//		ArrayList<ArrayList<String>> retList = new ArrayList<ArrayList<String>>();
//
//		ArrayList<String> list = new ArrayList<String>();
//		Cursor cursor = myDataBase.rawQuery(query, selectionArgs);
//	
//		if (cursor.moveToFirst()) {
//			do {
//				list = new ArrayList<String>();
//				for (int i = 0; i < cursor.getColumnCount(); i++) {
//					list.add(cursor.getString(i));
//				}
//				retList.add(list);
//			} while (cursor.moveToNext());
//		}
//		if (cursor != null && !cursor.isClosed()) {
//			cursor.close();
//		}
//		return retList;
//	}
//
//
//	// public ProductWrapper selectRecordsFromProduct(String query, String[]
//	// selectionArgs) {
//	//
//	//
//	// ProductWrapper _obj= new ProductWrapper();
//	// Cursor cursor = myDataBase.rawQuery(query, selectionArgs);
//	// if (cursor.moveToFirst()) {
//	//
//	// //list = new ArrayList<String>();
//	// //_obj= new Product_CanAddsWrapper();
//	// _obj.setPROD_Code (cursor.getString(1));
//	// _obj.setPROD_NameShort(cursor.getString(3));
//	// _obj.setPROD_Dim(cursor.getString(27));
//	//
//	// }
//	// if (cursor != null && !cursor.isClosed()) {
//	// cursor.close();
//	// }
//	// return _obj;
//	// }
//	// public ProductWrapper getProductPrice(String query, String[]
//	// selectionArgs) {
//	//
//	//
//	// ProductWrapper _obj= new ProductWrapper();
//	// Cursor cursor = myDataBase.rawQuery(query, selectionArgs);
//	// if (cursor.moveToFirst()) {
//	//
//	// //list = new ArrayList<String>();
//	// //_obj= new Product_CanAddsWrapper();
//	// _obj.setPROD_EatInPrice (cursor.getString(0));
//	// _obj.setPROD_TakeOutPrice(cursor.getString(1));
//	// _obj.setPROD_OtherPrice (cursor.getString(2));
//	//
//	// }
//	// if (cursor != null && !cursor.isClosed()) {
//	// cursor.close();
//	// }
//	// return _obj;
//	// }
//	// public int checkHasProduct(String query, String[] selectionArgs) {
//	//
//	//
//	//
//	// Cursor cursor = myDataBase.rawQuery(query, selectionArgs);
//	// int i=0;
//	// if (cursor.moveToFirst()) {
//	//
//	// //list = new ArrayList<String>();
//	// //_obj= new Product_CanAddsWrapper();
//	// i= Integer.parseInt(cursor.getString(0));
//	//
//	//
//	// }
//	// if (cursor != null && !cursor.isClosed()) {
//	// cursor.close();
//	// }
//	// return i;
//	// }
//	// public ArrayList <Product_CommentWrapper>
//	// selectRecordsFromProductIdComment(String query, String[] selectionArgs) {
//	//
//	// ArrayList<Product_CommentWrapper> retList = new
//	// ArrayList<Product_CommentWrapper>();
//	//
//	// ArrayList<String> list = new ArrayList<String>();
//	// Product_CommentWrapper _obj= new Product_CommentWrapper();
//	// Cursor cursor = myDataBase.rawQuery(query, selectionArgs);
//	// if (cursor.moveToFirst()) {
//	// do {
//	// //list = new ArrayList<String>();
//	// _obj= new Product_CommentWrapper();
//	// _obj.setProductCode(cursor.getString(1));
//	// _obj.setCOMM_ProdCode(cursor.getString(2));
//	// _obj.setCOMM_Flags(cursor.getString(3));
//	// _obj.setCOMM_MaxItem(cursor.getString(4));
//	// /*for(int i=0; i<cursor.getColumnCount(); i++){
//	// list.add( cursor.getString(i) );
//	// } */
//	// retList.add(_obj);
//	// } while (cursor.moveToNext());
//	// }
//	// if (cursor != null && !cursor.isClosed()) {
//	// cursor.close();
//	// }
//	// return retList;
//	// }
//	// // public ArrayList<BookWrapper> selectRecordsFromDBImg(String query,
//	// String[] selectionArgs) {
//	// //
//	// // ArrayList<BookWrapper> retList = new ArrayList<BookWrapper>();
//	// //
//	// // ArrayList<String> list = new ArrayList<String>();
//	// // BookWrapper _obj= new BookWrapper();
//	// // Cursor cursor = myDataBase.rawQuery(query, selectionArgs);
//	// // if (cursor.moveToFirst()) {
//	// // do {
//	// // //list = new ArrayList<String>();
//	// // _obj= new BookWrapper();
//	// // _obj.setUniqueID(cursor.getString(0));
//	// // _obj.setCoverImgURL(cursor.getString(1));
//	// // _obj.setCoverImg(cursor.getBlob(2));
//	// // /*for(int i=0; i<cursor.getColumnCount(); i++){
//	// // list.add( cursor.getString(i) );
//	// // } */
//	// // retList.add(_obj);
//	// // } while (cursor.moveToNext());
//	// // }
//	// // if (cursor != null && !cursor.isClosed()) {
//	// // cursor.close();
//	// // }
//	// // return retList;
//	// // }
//	/*public void getMyExhiList(String query, String[] selectionArgs) {
//		MyUIApplication._objExhibitorDirectoryList = null;
//		MyUIApplication._objExhibitorDirectoryList = new ArrayList<ExhibitorDirectoryWrapper>();
//
//		ExhibitorDirectoryWrapper _obj = new ExhibitorDirectoryWrapper();
//		Cursor cursor = myDataBase.rawQuery(query, selectionArgs);
//		if (cursor.moveToFirst()) {
//			do {
//
//				_obj = new ExhibitorDirectoryWrapper();
//				
//				 * _obj.setUniqueID(cursor.getString(1));
//				 * _obj.setThumbURL(cursor.getString(2));
//				 * _obj.setCoverImgURL(cursor.getString(3));
//				 * 
//				 * _obj.setThumbIMG(cursor.getBlob(4));
//				 
//
//				_obj.Name_id = (cursor.getString(0));
//				_obj.exhibitor_id = (cursor.getString(1));
//				_obj.name = (cursor.getString(2));
//				_obj.contact_person = (cursor.getString(3));
//				_obj.boothList = new ArrayList<String>();
//				_obj.boothList.add(cursor.getString(4));
//				_obj.boothList.add(cursor.getString(5));
//				_obj.boothList.add(cursor.getString(6));
//				_obj.boothList.add(cursor.getString(7));
//				_obj.boothList.add(cursor.getString(8));
//				_obj.boothList.add(cursor.getString(9));
//				_obj.boothList.add(cursor.getString(10));
//				_obj.boothList.add(cursor.getString(11));
//				_obj.hall = (cursor.getString(12));
//				_obj.url = (cursor.getString(13));
//				_obj.group = (cursor.getString(14));
//				_obj.section = (cursor.getString(15));
//				_obj.phone = (cursor.getString(16));
//				_obj.email = (cursor.getString(17));
//				_obj.Favourites = (cursor.getString(19));
//				_obj.Visited = (cursor.getString(20));
////				System.out.println(_obj.email);
//
//				MyUIApplication._objExhibitorDirectoryList.add(_obj);
//			} while (cursor.moveToNext());
//		}
//		if (cursor != null && !cursor.isClosed()) {
//			cursor.close();
//		}
//
//	}
//	public void getMyExhiDetail(String query, String[] selectionArgs) {
////		MyUIApplication._objExhibitorDirectoryList = null;
////		MyUIApplication._objExhibitorDirectoryList = new ArrayList<ExhibitorDirectoryWrapper>();
//		MyUIApplication._objExhibitorDirectoryWrapper = null;
//		MyUIApplication._objExhibitorDirectoryWrapper = new ExhibitorDirectoryWrapper();
//		Cursor cursor = myDataBase.rawQuery(query, selectionArgs);
//		if (cursor.moveToFirst()) {
//			do {
//				
//				MyUIApplication._objExhibitorDirectoryWrapper  = new ExhibitorDirectoryWrapper();
//				
//				 * _obj.setUniqueID(cursor.getString(1));
//				 * _obj.setThumbURL(cursor.getString(2));
//				 * _obj.setCoverImgURL(cursor.getString(3));
//				 * 
//				 * _obj.setThumbIMG(cursor.getBlob(4));
//				 
//				
//				MyUIApplication._objExhibitorDirectoryWrapper.Name_id = (cursor.getString(0));
//				MyUIApplication._objExhibitorDirectoryWrapper.exhibitor_id = (cursor.getString(1));
//				MyUIApplication._objExhibitorDirectoryWrapper.name = (cursor.getString(2));
//				MyUIApplication._objExhibitorDirectoryWrapper.contact_person = (cursor.getString(3));
//				MyUIApplication._objExhibitorDirectoryWrapper.boothList = new ArrayList<String>();
//				MyUIApplication._objExhibitorDirectoryWrapper.boothList.add(cursor.getString(4));
//				MyUIApplication._objExhibitorDirectoryWrapper.boothList.add(cursor.getString(5));
//				MyUIApplication._objExhibitorDirectoryWrapper.boothList.add(cursor.getString(6));
//				MyUIApplication._objExhibitorDirectoryWrapper.boothList.add(cursor.getString(7));
//				MyUIApplication._objExhibitorDirectoryWrapper.boothList.add(cursor.getString(8));
//				MyUIApplication._objExhibitorDirectoryWrapper.boothList.add(cursor.getString(9));
//				MyUIApplication._objExhibitorDirectoryWrapper.boothList.add(cursor.getString(10));
//				MyUIApplication._objExhibitorDirectoryWrapper.boothList.add(cursor.getString(11));
//				MyUIApplication._objExhibitorDirectoryWrapper.hall = (cursor.getString(12));
//				MyUIApplication._objExhibitorDirectoryWrapper.url = (cursor.getString(13));
//				MyUIApplication._objExhibitorDirectoryWrapper.group = (cursor.getString(14));
//				MyUIApplication._objExhibitorDirectoryWrapper.section = (cursor.getString(15));
//				MyUIApplication._objExhibitorDirectoryWrapper.phone = (cursor.getString(16));
//				MyUIApplication._objExhibitorDirectoryWrapper.email = (cursor.getString(17));
//				MyUIApplication._objExhibitorDirectoryWrapper.Favourites = (cursor.getString(19));
//				MyUIApplication._objExhibitorDirectoryWrapper.Visited = (cursor.getString(20));
////				System.out.println(_obj.email);
//				
//		//		MyUIApplication._objExhibitorDirectoryList.add(_obj);
//			} while (cursor.moveToNext());
//		}
//		if (cursor != null && !cursor.isClosed()) {
//			cursor.close();
//		}
//		
//	}
//	public void getMyNotesList(String query, String[] selectionArgs) {
//		MyUIApplication._objNotesList = null;
//		MyUIApplication._objNotesList = new ArrayList<NotesWrapper>();
//		
//		NotesWrapper _obj = new NotesWrapper();
//		Cursor cursor = myDataBase.rawQuery(query, selectionArgs);
//		if (cursor.moveToFirst()) {
//			do {
//				
//				_obj = new NotesWrapper();
//				
//				
//				_obj.Name_id = (cursor.getString(0));
//				_obj.id = (cursor.getString(1));
//				_obj.name = (cursor.getString(2));
//				_obj.contact_person = (cursor.getString(3));
//				_obj.note = (cursor.getString(4));
//				
//				MyUIApplication._objNotesList.add(_obj);
//				System.out.println("MyUIApplication._objNotesList>>"+MyUIApplication._objNotesList.size());
//			} while (cursor.moveToNext());
//		}
//		if (cursor != null && !cursor.isClosed()) {
//			cursor.close();
//		}
//		
//	}
//	public void getMyAppointmentList(String query, String[] selectionArgs) {
//		MyUIApplication._objAppointmentList = null;
//		MyUIApplication._objAppointmentList = new ArrayList<AppointmentWrapper>();
//		
//		AppointmentWrapper _obj = new AppointmentWrapper();
//		Cursor cursor = myDataBase.rawQuery(query, selectionArgs);
//		if (cursor.moveToFirst()) {
//			do {
//				
//				_obj = new AppointmentWrapper();
//				
//				
//				_obj.Name_id = (cursor.getString(0));
//				_obj.id = (cursor.getString(1));
//				_obj.name = (cursor.getString(2));
//				_obj.contact_person = (cursor.getString(3));
//				_obj.MobileNo = (cursor.getString(4));
//				_obj.Date = (cursor.getString(5));
//				_obj.Time = (cursor.getString(6));
//				
//				MyUIApplication._objAppointmentList.add(_obj);
//				System.out.println("MyUIApplication._objAppointmentList>>"+MyUIApplication._objAppointmentList.size());
//			} while (cursor.moveToNext());
//		}
//		if (cursor != null && !cursor.isClosed()) {
//			cursor.close();
//		}
//		
//	}
//	public void getSeminarEvents(String query, String[] selectionArgs) {
//
//		ArrayList<EventsWrapper> myTemplist = new ArrayList<EventsWrapper>();
//
//		EventsWrapper _obj = new EventsWrapper();
//		Cursor cursor = myDataBase.rawQuery(query, selectionArgs);
//		if (cursor.moveToFirst()) {
//			do {
//
//				_obj = new EventsWrapper();
//				
//				 * _obj.setUniqueID(cursor.getString(1));
//				 * _obj.setThumbURL(cursor.getString(2));
//				 * _obj.setCoverImgURL(cursor.getString(3));
//				 * 
//				 * _obj.setThumbIMG(cursor.getBlob(4));
//				 
//
//				_obj.Name_id = (cursor.getString(0));
//				_obj.id = (cursor.getString(1));
//				_obj.title = (cursor.getString(2));
//				_obj.description = cursor.getString(3);
//				_obj.section = cursor.getString(4);
//				_obj.date_time = (cursor.getString(6));
//				_obj.Last_Modify = (cursor.getString(7));
//
//				myTemplist.add(_obj);
//			} while (cursor.moveToNext());
//		}
//		if (cursor != null && !cursor.isClosed()) {
//			cursor.close();
//		}
//		MyUIApplication._objEventSeminarList.add(myTemplist);
//		myTemplist = null;
//
//	}
//
//	public void getNetworkEvents(String query, String[] selectionArgs) {
//
//		ArrayList<EventsWrapper> myTemplist = new ArrayList<EventsWrapper>();
//
//		EventsWrapper _obj = new EventsWrapper();
//		Cursor cursor = myDataBase.rawQuery(query, selectionArgs);
//		if (cursor.moveToFirst()) {
//			do {
//
//				_obj = new EventsWrapper();
//				
//				 * _obj.setUniqueID(cursor.getString(1));
//				 * _obj.setThumbURL(cursor.getString(2));
//				 * _obj.setCoverImgURL(cursor.getString(3));
//				 * 
//				 * _obj.setThumbIMG(cursor.getBlob(4));
//				 
//
//				_obj.Name_id = (cursor.getString(0));
//				_obj.id = (cursor.getString(1));
//				_obj.title = (cursor.getString(2));
//				_obj.description = cursor.getString(3);
//				_obj.section = cursor.getString(4);
//				_obj.date_time = (cursor.getString(6));
//				_obj.Last_Modify = (cursor.getString(7));
//
//				System.out.println("_obj.description = cursor.getString(3); "
//						+ _obj.description);
//
//				myTemplist.add(_obj);
//			} while (cursor.moveToNext());
//		}
//		if (cursor != null && !cursor.isClosed()) {
//			cursor.close();
//		}
//		MyUIApplication._objEventNetworkList.add(myTemplist);
//		myTemplist = null;
//
//	}
//	public void getEvents(String query, String[] selectionArgs) {
//		MyUIApplication._objEventList = null;
//		MyUIApplication._objEventList = new ArrayList<EventsWrapper>();
//
//		EventsWrapper _obj = new EventsWrapper();
//		Cursor cursor = myDataBase.rawQuery(query, selectionArgs);
//		if (cursor.moveToFirst()) {
//			do {
//
//				_obj = new EventsWrapper();
//				
//				 * _obj.setUniqueID(cursor.getString(1));
//				 * _obj.setThumbURL(cursor.getString(2));
//				 * _obj.setCoverImgURL(cursor.getString(3));
//				 * 
//				 * _obj.setThumbIMG(cursor.getBlob(4));
//				 
//
//				_obj.Name_id = (cursor.getString(0));
//				_obj.id = (cursor.getString(1));
//				_obj.title = (cursor.getString(2));
//				_obj.description = cursor.getString(3);
//				_obj.section = cursor.getString(4);
//				_obj.date_time = (cursor.getString(6));
//				_obj.Last_Modify = (cursor.getString(7));
//
//				MyUIApplication._objEventList.add(_obj);
//			} while (cursor.moveToNext());
//		}
//		if (cursor != null && !cursor.isClosed()) {
//			cursor.close();
//		}
//
//	}
//	public void getSeminarDateEvents(String query, String[] selectionArgs) {
//		MyUIApplication._objDateEventSeminarList = null;
//		MyUIApplication._objDateEventSeminarList = new ArrayList<String>();
//
//		Cursor cursor = myDataBase.rawQuery(query, selectionArgs);
//		if (cursor.moveToFirst()) {
//			do {
//
//				MyUIApplication._objDateEventSeminarList.add(cursor
//						.getString(0));
//			} while (cursor.moveToNext());
//		}
//		if (cursor != null && !cursor.isClosed()) {
//			cursor.close();
//		}
//
//	}
//	public void getNetworkDateEvents(String query, String[] selectionArgs) {
//		MyUIApplication._objDateEventNetworkingList = null;
//		MyUIApplication._objDateEventNetworkingList = new ArrayList<String>();
//
//		Cursor cursor = myDataBase.rawQuery(query, selectionArgs);
//		if (cursor.moveToFirst()) {
//			do {
//
//				MyUIApplication._objDateEventNetworkingList.add(cursor
//						.getString(0));
//			} while (cursor.moveToNext());
//		}
//		if (cursor != null && !cursor.isClosed()) {
//			cursor.close();
//		}
//
//	}
//	public SyacTimeWrapper getMySyncTime(String query, String[] selectionArgs) {
//
//		SyacTimeWrapper _obj = new SyacTimeWrapper();
//		Cursor cursor = myDataBase.rawQuery(query, selectionArgs);
//		if (cursor.moveToFirst()) {
//			do {
//
//				_obj = new SyacTimeWrapper();
//				
//				 * _obj.setUniqueID(cursor.getString(1));
//				 * _obj.setThumbURL(cursor.getString(2));
//				 * _obj.setCoverImgURL(cursor.getString(3));
//				 * 
//				 * _obj.setThumbIMG(cursor.getBlob(4));
//				 
//
//				_obj.id = (cursor.getString(0));
//				_obj.name = (cursor.getString(1));
//
//				_obj.Date_time = (cursor.getString(2));
//
//			} while (cursor.moveToNext());
//		}
//		if (cursor != null && !cursor.isClosed()) {
//			cursor.close();
//		}
//		return _obj;
//
//	}
//	public void getShowUpdatesDate(String query, String[] selectionArgs) {
//		MyUIApplication._objDateShowUpdateList = null;
//		MyUIApplication._objDateShowUpdateList = new ArrayList<String>();
//
//		Cursor cursor = myDataBase.rawQuery(query, selectionArgs);
//		if (cursor.moveToFirst()) {
//			do {
//
//				MyUIApplication._objDateShowUpdateList.add(cursor.getString(0));
//			} while (cursor.moveToNext());
//		}
//		if (cursor != null && !cursor.isClosed()) {
//			cursor.close();
//		}
//
//	}
//	public void getShowUpdateDate(String query, String[] selectionArgs) {
//
//		ArrayList<ShowUpdatesWrapper> myTemplist = new ArrayList<ShowUpdatesWrapper>();
//
//		ShowUpdatesWrapper _obj = new ShowUpdatesWrapper();
//		Cursor cursor = myDataBase.rawQuery(query, selectionArgs);
//		if (cursor.moveToFirst()) {
//			do {
//
//				_obj = new ShowUpdatesWrapper();
//				
//				 * _obj.setUniqueID(cursor.getString(1));
//				 * _obj.setThumbURL(cursor.getString(2));
//				 * _obj.setCoverImgURL(cursor.getString(3));
//				 * 
//				 * _obj.setThumbIMG(cursor.getBlob(4));
//				 
//
//				_obj.Name_id = (cursor.getString(0));
//				_obj.id = (cursor.getString(1));
//				_obj.title = (cursor.getString(2));
//				_obj.description = cursor.getString(3);
//				_obj.date_time = (cursor.getString(4));
//
//				myTemplist.add(_obj);
//			} while (cursor.moveToNext());
//		}
//		if (cursor != null && !cursor.isClosed()) {
//			cursor.close();
//		}
//		MyUIApplication._objShowUpdateList.add(myTemplist);
//		System.out.println(MyUIApplication._objShowUpdateList.size());
//		myTemplist = null;
//
//	}
//	public void getUpdate(String query, String[] selectionArgs) {
//		MyUIApplication._objShow_UpdatesNew = null;
//		MyUIApplication._objShow_UpdatesNew = new ArrayList<ShowUpdatesWrapper>();
//
//		ShowUpdatesWrapper _obj = new ShowUpdatesWrapper();
//		Cursor cursor = myDataBase.rawQuery(query, selectionArgs);
//		if (cursor.moveToFirst()) {
//			do {
//
//				_obj = new ShowUpdatesWrapper();
//				
//				 * _obj.setUniqueID(cursor.getString(1));
//				 * _obj.setThumbURL(cursor.getString(2));
//				 * _obj.setCoverImgURL(cursor.getString(3));
//				 * 
//				 * _obj.setThumbIMG(cursor.getBlob(4));
//				 
//
//				_obj.Name_id = (cursor.getString(0));
//				_obj.id = (cursor.getString(1));
//				_obj.title = (cursor.getString(2));
//				_obj.description = cursor.getString(3);
//				_obj.date_time = (cursor.getString(4));
//
//				MyUIApplication._objShow_UpdatesNew.add(_obj);
//			} while (cursor.moveToNext());
//		}
//		if (cursor != null && !cursor.isClosed()) {
//			cursor.close();
//		}
//
//	}
//	public void getZoneManager1(String query, String[] selectionArgs) {
//		MyUIApplication._objZoneManagerList1 = null;
//		MyUIApplication._objZoneManagerList1 = new ArrayList<ZoneManagerWrapper>();
//
//		ZoneManagerWrapper _obj = new ZoneManagerWrapper();
//		Cursor cursor = myDataBase.rawQuery(query, selectionArgs);
//		if (cursor.moveToFirst()) {
//			do {
//
//				_obj = new ZoneManagerWrapper();
//				
//				 * _obj.setUniqueID(cursor.getString(1));
//				 * _obj.setThumbURL(cursor.getString(2));
//				 * _obj.setCoverImgURL(cursor.getString(3));
//				 * 
//				 * _obj.setThumbIMG(cursor.getBlob(4));
//				 
//
//				_obj.Name_id = (cursor.getString(0));
//				_obj.id = (cursor.getString(1));
//				_obj.hall_no = (cursor.getString(2));
//				_obj.zone_no = cursor.getString(3);
//				_obj.contact_person = cursor.getString(4);
//				_obj.mobile_no = (cursor.getString(5));
//				_obj.email_id = (cursor.getString(6));
//				_obj.date_time = (cursor.getString(7));
//				_obj.last_modify = (cursor.getString(8));
//				MyUIApplication._objZoneManagerList1.add(_obj);
//			} while (cursor.moveToNext());
//		}
//		if (cursor != null && !cursor.isClosed()) {
//			cursor.close();
//		}
//
//	}
//
//	public void getZoneManager2(String query, String[] selectionArgs) {
//		MyUIApplication._objZoneManagerList2 = null;
//		MyUIApplication._objZoneManagerList2 = new ArrayList<ZoneManagerWrapper>();
//
//		ZoneManagerWrapper _obj = new ZoneManagerWrapper();
//		Cursor cursor = myDataBase.rawQuery(query, selectionArgs);
//		if (cursor.moveToFirst()) {
//			do {
//
//				_obj = new ZoneManagerWrapper();
//				
//				 * _obj.setUniqueID(cursor.getString(1));
//				 * _obj.setThumbURL(cursor.getString(2));
//				 * _obj.setCoverImgURL(cursor.getString(3));
//				 * 
//				 * _obj.setThumbIMG(cursor.getBlob(4));
//				 
//
//				_obj.Name_id = (cursor.getString(0));
//				_obj.id = (cursor.getString(1));
//				_obj.hall_no = (cursor.getString(2));
//				_obj.zone_no = cursor.getString(3);
//				_obj.contact_person = cursor.getString(4);
//				_obj.mobile_no = (cursor.getString(5));
//				_obj.email_id = (cursor.getString(6));
//				_obj.date_time = (cursor.getString(7));
//				_obj.last_modify = (cursor.getString(8));
//				MyUIApplication._objZoneManagerList2.add(_obj);
//			} while (cursor.moveToNext());
//		}
//		if (cursor != null && !cursor.isClosed()) {
//			cursor.close();
//		}
//
//	}
//
//	public void getZoneManager5(String query, String[] selectionArgs) {
//		MyUIApplication._objZoneManagerList5 = null;
//		MyUIApplication._objZoneManagerList5 = new ArrayList<ZoneManagerWrapper>();
//
//		ZoneManagerWrapper _obj = new ZoneManagerWrapper();
//		Cursor cursor = myDataBase.rawQuery(query, selectionArgs);
//		if (cursor.moveToFirst()) {
//			do {
//
//				_obj = new ZoneManagerWrapper();
//				
//				 * _obj.setUniqueID(cursor.getString(1));
//				 * _obj.setThumbURL(cursor.getString(2));
//				 * _obj.setCoverImgURL(cursor.getString(3));
//				 * 
//				 * _obj.setThumbIMG(cursor.getBlob(4));
//				 
//
//				_obj.Name_id = (cursor.getString(0));
//				_obj.id = (cursor.getString(1));
//				_obj.hall_no = (cursor.getString(2));
//				_obj.zone_no = cursor.getString(3);
//				_obj.contact_person = cursor.getString(4);
//				_obj.mobile_no = (cursor.getString(5));
//				_obj.email_id = (cursor.getString(6));
//				_obj.date_time = (cursor.getString(7));
//				_obj.last_modify = (cursor.getString(8));
//				MyUIApplication._objZoneManagerList5.add(_obj);
//			} while (cursor.moveToNext());
//		}
//		if (cursor != null && !cursor.isClosed()) {
//			cursor.close();
//		}
//
//	}
//
//	public void getZoneManager6(String query, String[] selectionArgs) {
//		MyUIApplication._objZoneManagerList6 = null;
//		MyUIApplication._objZoneManagerList6 = new ArrayList<ZoneManagerWrapper>();
//
//		ZoneManagerWrapper _obj = new ZoneManagerWrapper();
//		Cursor cursor = myDataBase.rawQuery(query, selectionArgs);
//		if (cursor.moveToFirst()) {
//			do {
//
//				_obj = new ZoneManagerWrapper();
//				
//				 * _obj.setUniqueID(cursor.getString(1));
//				 * _obj.setThumbURL(cursor.getString(2));
//				 * _obj.setCoverImgURL(cursor.getString(3));
//				 * 
//				 * _obj.setThumbIMG(cursor.getBlob(4));
//				 
//
//				_obj.Name_id = (cursor.getString(0));
//				_obj.id = (cursor.getString(1));
//				_obj.hall_no = (cursor.getString(2));
//				_obj.zone_no = cursor.getString(3);
//				_obj.contact_person = cursor.getString(4);
//				_obj.mobile_no = (cursor.getString(5));
//				_obj.email_id = (cursor.getString(6));
//				_obj.date_time = (cursor.getString(7));
//				_obj.last_modify = (cursor.getString(8));
//				MyUIApplication._objZoneManagerList6.add(_obj);
//			} while (cursor.moveToNext());
//		}
//		if (cursor != null && !cursor.isClosed()) {
//			cursor.close();
//		}
//
//	}
//
//
//	public void deleteAll(String tablename) {
//		myDataBase.delete(tablename, null, null);
//
//	}*/
//}

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * 
 * @author dev A helper class to manage database creation and version
 *         management.
 *
 */
public class DBAdapter extends SQLiteOpenHelper {

	// SQLiteDatabase has methods to create, delete, execute SQL commands, and
	// perform other common database management tasks.
	private SQLiteDatabase myDataBase;
	static public DBAdapter instance;

	// public static final String DATABASE_FILE_PATH =
	// Environment.getExternalStorageDirectory();;
	private static final int DATABASE_VERSION = 1;
	// Database Name
	private static final String DATABASE_NAME = "forsk.db";

	public static final String FACULTY_TABLE_NAME = "FACULTY";
	public static final String FACULTY_COLUMN_ID = "_id";
	public static final String FACULTY_COLUMN_FIRSTNAME = "first_name";
	public static final String FACULTY_COLUMN_LASTNAME = "last_name";
	public static final String FACULTY_COLUMN_PHOTO = "photo";
	public static final String FACULTY_COLUMN_DEP = "department";
	public static final String FACULTY_COLUMN_PHONE = "phone";
	public static final String FACULTY_COLUMN_EMAIL = "email";

	// we need to maintain a single instance of the database connection class
	// since multipal threads will be using this connection
	public static DBAdapter getInstance(Context context) {
		if (instance == null) {
			instance = new DBAdapter(context);
		}
		return instance;
	}

	private DBAdapter(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		// super(context, Environment.getExternalStorageDirectory() +
		// File.separator + DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		String CREATE_TABLE_FACULTY = "CREATE TABLE " + FACULTY_TABLE_NAME + "(" + FACULTY_COLUMN_ID + " INTEGER PRIMARY KEY," + FACULTY_COLUMN_FIRSTNAME + " TEXT," + FACULTY_COLUMN_LASTNAME
				+ " TEXT," + FACULTY_COLUMN_PHOTO + " TEXT," + FACULTY_COLUMN_DEP + " TEXT," + FACULTY_COLUMN_PHONE + " TEXT," + FACULTY_COLUMN_EMAIL + " TEXT)";
		db.execSQL(CREATE_TABLE_FACULTY);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		// on upgrade drop older tables
		db.execSQL("DROP TABLE IF EXISTS " + FACULTY_TABLE_NAME);
		// create new tables
		onCreate(db);
	}

	/**
	 * This function used to select the records from DB.
	 * 
	 * @param tableName
	 * @param tableColumns
	 * @param whereClase
	 * @param whereArgs
	 * @param groupBy
	 * @param having
	 * @param orderBy
	 * @return A Cursor object, which is positioned before the first entry.
	 */
	public Cursor selectRecordsFromDB(String tableName, String[] tableColumns, String whereClase, String whereArgs[], String groupBy, String having, String orderBy) {

		myDataBase = this.getWritableDatabase();

		return myDataBase.query(tableName, tableColumns, whereClase, whereArgs, groupBy, having, orderBy);
	}

	/**
	 * This function used to insert the Record in DB.
	 * 
	 * @param tableName
	 * @param nullColumnHack
	 * @param initialValues
	 * @return the row ID of the newly inserted row, or -1 if an error occurred
	 */
	public long insertRecordsInDB(String tableName, String nullColumnHack, ContentValues initialValues) {

		myDataBase = this.getWritableDatabase();

		return myDataBase.insert(tableName, nullColumnHack, initialValues);
	}

	/**
	 * This function used to update the Record in DB.
	 * 
	 * @param tableName
	 * @param initialValues
	 * @param whereClause
	 * @param whereArgs
	 * @return true / false on updating one or more records
	 */
	public boolean updateRecordInDB(String tableName, ContentValues initialValues, String whereClause, String whereArgs[]) {

		myDataBase = this.getWritableDatabase();

		return myDataBase.update(tableName, initialValues, whereClause, whereArgs) > 0;
	}

	/**
	 * This function used to update the Record in DB.
	 * 
	 * @param tableName
	 * @param initialValues
	 * @param whereClause
	 * @param whereArgs
	 * @return 0 in case of failure otherwise return no of row(s) are updated
	 */
	public int updateRecordsInDB(String tableName, ContentValues initialValues, String whereClause, String whereArgs[]) {
		myDataBase = this.getWritableDatabase();

		return myDataBase.update(tableName, initialValues, whereClause, whereArgs);
	}

	/**
	 * This function used to delete the Record in DB.
	 * 
	 * @param tableName
	 * @param whereClause
	 * @param whereArgs
	 * @return 0 in case of failure otherwise return no of row(s) are deleted.
	 */
	public int deleteRecordInDB(String tableName, String whereClause, String[] whereArgs) {

		myDataBase = this.getWritableDatabase();

		return myDataBase.delete(tableName, whereClause, whereArgs);
	}

	// --------------------- Select Raw Query Functions ---------------------

	/**
	 * apply raw Query
	 * 
	 * @param query
	 * @param selectionArgs
	 * @return Cursor
	 */
	public Cursor selectRecordsFromDB(String query, String[] selectionArgs) {
		myDataBase = this.getWritableDatabase();

		return myDataBase.rawQuery(query, selectionArgs);
	}

	/**
	 * apply raw query and return result in list
	 * 
	 * @param query
	 * @param selectionArgs
	 * @return ArrayList<ArrayList<String>>
	 */
	public ArrayList<ArrayList<String>> selectRecordsFromDBList(String query, String[] selectionArgs) {

		myDataBase = this.getWritableDatabase();

		ArrayList<ArrayList<String>> retList = new ArrayList<ArrayList<String>>();

		ArrayList<String> list = new ArrayList<String>();
		Cursor cursor = myDataBase.rawQuery(query, selectionArgs);

		if (cursor.moveToFirst()) {
			do {
				list = new ArrayList<String>();
				for (int i = 0; i < cursor.getColumnCount(); i++) {
					list.add(cursor.getString(i));
				}
				retList.add(list);
			} while (cursor.moveToNext());
		}
		if (cursor != null && !cursor.isClosed()) {
			cursor.close();
		}
		return retList;
	}

}
