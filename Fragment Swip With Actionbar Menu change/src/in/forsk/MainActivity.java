package in.forsk;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

public class MainActivity extends Activity {
	// alt+shift+s to open override/implemented methods of super class
	FragmentManager fManager;
	FragmentTransaction fTrans;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		if (savedInstanceState == null) {
			// Usage of singleton class concept
			fManager = getFragmentManager();
			fTrans = fManager.beginTransaction();
			fTrans.add(R.id.container, new PlaceholderFragment());
			fTrans.commit();
			invalidateOptionsMenu();
		}
	}

	// @Override
	// public boolean onCreateOptionsMenu(Menu menu) {
	// // Inflate the menu; this adds items to the action bar if it is present.
	// getMenuInflater().inflate(R.menu.main, menu);
	// return true;
	// }
	//
	// @Override
	// public boolean onOptionsItemSelected(MenuItem item) {
	// // Handle action bar item clicks here. The action bar will
	// // automatically handle clicks on the Home/Up button, so long
	// // as you specify a parent activity in AndroidManifest.xml.
	// int id = item.getItemId();
	// if (id == R.id.action_settings) {
	// return true;
	// }
	// return super.onOptionsItemSelected(item);
	// }

	/**
	 * A placeholder fragment containing a simple view. Fragment inner static
	 * class
	 */
	public static class PlaceholderFragment extends Fragment {

		// Ctrl+shift+O == auto import
		Button btn1;

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container, false);

			btn1 = (Button) rootView.findViewById(R.id.button1);

			btn1.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					getActivity().getFragmentManager().beginTransaction().replace(R.id.container, new SecondFragment()).commit();
					getActivity().invalidateOptionsMenu();
				}
			});
			setHasOptionsMenu(true);
			return rootView;
		}

		@Override
		public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
			inflater.inflate(R.menu.first_fragment_menu, menu);
			// menu.clear();
			// fragment specific menu creation
		}

		@Override
		public boolean onOptionsItemSelected(MenuItem item) {
			int id = item.getItemId();
			if (id == R.id.first_frag_menu) {
				getActivity().getFragmentManager().beginTransaction().replace(R.id.container, new SecondFragment()).commit();
				getActivity().invalidateOptionsMenu();
				return true;
			}
			return super.onOptionsItemSelected(item);
		}

	}

}
